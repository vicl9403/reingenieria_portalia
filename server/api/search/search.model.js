'use strict';

import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var SearchSchema = new mongoose.Schema({
  user: { type: Schema.ObjectId, ref: 'User' },
  term: { type: String, required: true },
  createdAt: { type: Date, required:true, default: Date.now() },
  hits: { type: Number },
  hasClicked: { type: Boolean, default: 0 },
});

export default mongoose.model('Search', SearchSchema);
