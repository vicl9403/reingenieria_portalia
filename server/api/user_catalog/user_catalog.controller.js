/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/user_catalogs              ->  index
 * POST    /api/user_catalogs              ->  create
 * GET     /api/user_catalogs/:id          ->  show
 * PUT     /api/user_catalogs/:id          ->  upsert
 * PATCH   /api/user_catalogs/:id          ->  patch
 * DELETE  /api/user_catalogs/:id          ->  destroy
 */

'use strict';

import { applyPatch } from 'fast-json-patch';
import UserCatalog from './user_catalog.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      applyPatch(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => res.status(204).end());
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of UserCatalogs
export function index(req, res) {
  return UserCatalog
    .find()
    .populate('user')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single UserCatalog from the DB
export function show(req, res) {
  return UserCatalog.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new UserCatalog in the DB
export function create(req, res) {
  return UserCatalog.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given UserCatalog in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return UserCatalog.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing UserCatalog in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return UserCatalog.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a UserCatalog from the DB
export function destroy(req, res) {
  return UserCatalog.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
