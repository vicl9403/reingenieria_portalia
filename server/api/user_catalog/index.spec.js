'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var userCatalogCtrlStub = {
  index: 'userCatalogCtrl.index',
  show: 'userCatalogCtrl.show',
  create: 'userCatalogCtrl.create',
  upsert: 'userCatalogCtrl.upsert',
  patch: 'userCatalogCtrl.patch',
  destroy: 'userCatalogCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var userCatalogIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './user_catalog.controller': userCatalogCtrlStub
});

describe('UserCatalog API Router:', function() {
  it('should return an express router instance', function() {
    userCatalogIndex.should.equal(routerStub);
  });

  describe('GET /api/user_catalogs', function() {
    it('should route to userCatalog.controller.index', function() {
      routerStub.get
        .withArgs('/', 'userCatalogCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/user_catalogs/:id', function() {
    it('should route to userCatalog.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'userCatalogCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/user_catalogs', function() {
    it('should route to userCatalog.controller.create', function() {
      routerStub.post
        .withArgs('/', 'userCatalogCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/user_catalogs/:id', function() {
    it('should route to userCatalog.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'userCatalogCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/user_catalogs/:id', function() {
    it('should route to userCatalog.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'userCatalogCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/user_catalogs/:id', function() {
    it('should route to userCatalog.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'userCatalogCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
