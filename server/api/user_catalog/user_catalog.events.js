/**
 * UserCatalog model events
 */

'use strict';

import {EventEmitter} from 'events';
var UserCatalogEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
UserCatalogEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(UserCatalog) {
  for(var e in events) {
    let event = events[e];
    UserCatalog.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    UserCatalogEvents.emit(event + ':' + doc._id, doc);
    UserCatalogEvents.emit(event, doc);
  };
}

export {registerEvents};
export default UserCatalogEvents;
