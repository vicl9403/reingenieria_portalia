'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newUserCatalog;

describe('UserCatalog API:', function() {
  describe('GET /api/user_catalogs', function() {
    var userCatalogs;

    beforeEach(function(done) {
      request(app)
        .get('/api/user_catalogs')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          userCatalogs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      userCatalogs.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/user_catalogs', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/user_catalogs')
        .send({
          name: 'New UserCatalog',
          info: 'This is the brand new userCatalog!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newUserCatalog = res.body;
          done();
        });
    });

    it('should respond with the newly created userCatalog', function() {
      newUserCatalog.name.should.equal('New UserCatalog');
      newUserCatalog.info.should.equal('This is the brand new userCatalog!!!');
    });
  });

  describe('GET /api/user_catalogs/:id', function() {
    var userCatalog;

    beforeEach(function(done) {
      request(app)
        .get(`/api/user_catalogs/${newUserCatalog._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          userCatalog = res.body;
          done();
        });
    });

    afterEach(function() {
      userCatalog = {};
    });

    it('should respond with the requested userCatalog', function() {
      userCatalog.name.should.equal('New UserCatalog');
      userCatalog.info.should.equal('This is the brand new userCatalog!!!');
    });
  });

  describe('PUT /api/user_catalogs/:id', function() {
    var updatedUserCatalog;

    beforeEach(function(done) {
      request(app)
        .put(`/api/user_catalogs/${newUserCatalog._id}`)
        .send({
          name: 'Updated UserCatalog',
          info: 'This is the updated userCatalog!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedUserCatalog = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedUserCatalog = {};
    });

    it('should respond with the updated userCatalog', function() {
      updatedUserCatalog.name.should.equal('Updated UserCatalog');
      updatedUserCatalog.info.should.equal('This is the updated userCatalog!!!');
    });

    it('should respond with the updated userCatalog on a subsequent GET', function(done) {
      request(app)
        .get(`/api/user_catalogs/${newUserCatalog._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let userCatalog = res.body;

          userCatalog.name.should.equal('Updated UserCatalog');
          userCatalog.info.should.equal('This is the updated userCatalog!!!');

          done();
        });
    });
  });

  describe('PATCH /api/user_catalogs/:id', function() {
    var patchedUserCatalog;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/user_catalogs/${newUserCatalog._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched UserCatalog' },
          { op: 'replace', path: '/info', value: 'This is the patched userCatalog!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedUserCatalog = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedUserCatalog = {};
    });

    it('should respond with the patched userCatalog', function() {
      patchedUserCatalog.name.should.equal('Patched UserCatalog');
      patchedUserCatalog.info.should.equal('This is the patched userCatalog!!!');
    });
  });

  describe('DELETE /api/user_catalogs/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/user_catalogs/${newUserCatalog._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when userCatalog does not exist', function(done) {
      request(app)
        .delete(`/api/user_catalogs/${newUserCatalog._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
