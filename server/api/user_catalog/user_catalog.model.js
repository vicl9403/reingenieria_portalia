'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './user_catalog.events';

const Schema = mongoose.Schema;

var UserCatalogSchema = new mongoose.Schema({

  // Datos generales del catálogo
  name: { type: String, required: true },
  content: { type: String, required: true },
  ext: { type: String, required: true },

  // Relación con el usuario
  user: { type: Schema.ObjectId, ref: 'User' }

});

registerEvents(UserCatalogSchema);
export default mongoose.model('UserCatalog', UserCatalogSchema);
