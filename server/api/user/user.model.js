'use strict';

import mongoose from 'mongoose';

import {registerEvents} from './user.events';

const Schema = mongoose.Schema;

const uniqueValidator = require('mongoose-unique-validator');

var UserSchema = new mongoose.Schema({
  // Datos personales
  name: { type: String, required:true },
  phones: { type: Object },
  email: { type: String, required:true, unique:true},
  password: { type: String, required: true },
  score: { type: Number, default: 4 },
  profilePic: { type: String },
  enableNotifications: { type: Boolean, default: false },
  contactName: { type: String },

  //Datos de empresa
  videos: { type: Array },
  tags: { type: Array },

  //Datos fiscales
  reason: { type: String },
  billingInfo: {
    rfc: { type: String },
    address: { type: String },
    zipCode: { type: Number },
    email: { type: String }
  },

  //Datos de contacto y redes sociales
  webpage: { type: String },

  //Datos adicionales
  views: { type: Number },
  rating: { type: Number, default: 4 }, // Dato que se calcula con un trigger
  role: { type: String, required: true, default: 'user' },

  schedules: {
    monday: { type: String },
    tuesday: { type: String },
    wednesday: { type: String },
    thursday: { type: String },
    friday: { type: String },
    saturday: { type: String },
    sunday: { type: String }
  },

  // Relación hacia el modelo de Catálogos del usuario
  catalogs: [{ type: Schema.ObjectId, ref: 'UserCatalog'}]

});

UserSchema.plugin(uniqueValidator);

registerEvents(UserSchema);
export default mongoose.model('User', UserSchema);
