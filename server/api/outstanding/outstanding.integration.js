'use strict';

var app = require('../..');
import request from 'supertest';

var newOutstanding;

describe('Outstanding API:', function() {

  describe('GET /api/outstandings', function() {
    var outstandings;

    beforeEach(function(done) {
      request(app)
        .get('/api/outstandings')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          outstandings = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      outstandings.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/outstandings', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/outstandings')
        .send({
          name: 'New Outstanding',
          info: 'This is the brand new outstanding!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newOutstanding = res.body;
          done();
        });
    });

    it('should respond with the newly created outstanding', function() {
      newOutstanding.name.should.equal('New Outstanding');
      newOutstanding.info.should.equal('This is the brand new outstanding!!!');
    });

  });

  describe('GET /api/outstandings/:id', function() {
    var outstanding;

    beforeEach(function(done) {
      request(app)
        .get('/api/outstandings/' + newOutstanding._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          outstanding = res.body;
          done();
        });
    });

    afterEach(function() {
      outstanding = {};
    });

    it('should respond with the requested outstanding', function() {
      outstanding.name.should.equal('New Outstanding');
      outstanding.info.should.equal('This is the brand new outstanding!!!');
    });

  });

  describe('PUT /api/outstandings/:id', function() {
    var updatedOutstanding;

    beforeEach(function(done) {
      request(app)
        .put('/api/outstandings/' + newOutstanding._id)
        .send({
          name: 'Updated Outstanding',
          info: 'This is the updated outstanding!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedOutstanding = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedOutstanding = {};
    });

    it('should respond with the updated outstanding', function() {
      updatedOutstanding.name.should.equal('Updated Outstanding');
      updatedOutstanding.info.should.equal('This is the updated outstanding!!!');
    });

  });

  describe('DELETE /api/outstandings/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/outstandings/' + newOutstanding._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when outstanding does not exist', function(done) {
      request(app)
        .delete('/api/outstandings/' + newOutstanding._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
