'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var outstandingCtrlStub = {
  index: 'outstandingCtrl.index',
  show: 'outstandingCtrl.show',
  create: 'outstandingCtrl.create',
  update: 'outstandingCtrl.update',
  destroy: 'outstandingCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var outstandingIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './outstanding.controller': outstandingCtrlStub
});

describe('Outstanding API Router:', function() {

  it('should return an express router instance', function() {
    outstandingIndex.should.equal(routerStub);
  });

  describe('GET /api/outstandings', function() {

    it('should route to outstanding.controller.index', function() {
      routerStub.get
        .withArgs('/', 'outstandingCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/outstandings/:id', function() {

    it('should route to outstanding.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'outstandingCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/outstandings', function() {

    it('should route to outstanding.controller.create', function() {
      routerStub.post
        .withArgs('/', 'outstandingCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/outstandings/:id', function() {

    it('should route to outstanding.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'outstandingCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/outstandings/:id', function() {

    it('should route to outstanding.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'outstandingCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/outstandings/:id', function() {

    it('should route to outstanding.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'outstandingCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
