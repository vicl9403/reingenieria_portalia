'use strict';

import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var OutstandingSchema = new mongoose.Schema({
  user: { type: Schema.ObjectId, ref: 'User', required: true },
  since: { type: Date, required: true },
  until: { type: Date, required:true },
});

export default mongoose.model('Outstanding', OutstandingSchema);
