/**
 * Outstanding model events
 */

'use strict';

import {EventEmitter} from 'events';
import Outstanding from './outstanding.model';
var OutstandingEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
OutstandingEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Outstanding.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    OutstandingEvents.emit(event + ':' + doc._id, doc);
    OutstandingEvents.emit(event, doc);
  }
}

export default OutstandingEvents;
