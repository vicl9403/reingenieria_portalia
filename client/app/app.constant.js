(function(angular, undefined) {
  angular.module("portaliaUmgApp.constants", [])

.constant("appConfig", {
	"userRoles": [
		"guest",
		"user",
		"admin"
	]
})

;
})(angular);